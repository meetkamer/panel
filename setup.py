from setuptools import setup

setup(name='panels',
      version='0.1',
      description='Panels Library',
      url='http://bitbucket.com',
      license='MIT',
      packages=['panels'],
      zip_safe=False
      )

