
from panels.parameter import parameter, parameter_container

class P(parameter_container):
    x1 = parameter()
    x2 = parameter(default=3)
    x3 = parameter(default=[])

p = P(x3='a')    
p.x1

