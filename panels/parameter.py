import collections

class constant(int):
    pass

'''
INVALID = constant(1)
LOCKED = constant(2)
DISABLED = constant(4)
ABSENT = constant(8)
MUST_INIT = constant(16)
ENABLED = constant(100)
'''
class MUST_INIT:
    pass

class EXCEPTION:
    pass
class ABSENT(EXCEPTION):
    pass
ABSENT_INSTANCE = ABSENT()    
class INVALID(EXCEPTION):
    pass
class LOCKED(EXCEPTION):
    pass


class ValueError(ValueError):
    pass
    #def __init__(self, what, value):
    #    super(ValueError, self).__init__(self, what)

class parameter_base__:

    INVALID = INVALID
    LOCKED = LOCKED
    ABSENT = ABSENT    
    ValueError = ValueError

    '''
    class parameter_state(list):
        def __init__(self, *args):
            self.extend(args)
        def value(self):
            if self[0] & INVALID:
                return INVALID            
            return self[1]
    '''

    def __init__(self, label = '', default = MUST_INIT, doc = '', callback = None, **kwargs):
        self.label = label
        self.default = default
        self.doc = doc
        self.callback = callback
        if kwargs:
            unknown_args = ", ".join(kwargs.keys())
            raise RuntimeError("Unknown argument(s): " + unknown_args)

    '''
    def __init__(self, **kwargs):
        self.label = kwargs.pop("label", ABSENT)
        self.default = kwargs.pop("default", ABSENT)
        self.callback = kwargs.pop("callback", None)
        self.doc = kwargs.pop("doc", "")
        if kwargs:
            unknown_args = ", ".join(kwargs.keys())
            raise RuntimeError("Unknown argument(s): " + unknown_args)
    '''

    def __get__(self, obj, type=None):
        if obj is None:
            return self
        v = obj.__values__[self]
        if v is ABSENT_INSTANCE:
            raise ValueError("%r has not been set"%(self.name,))
        return v

    def __set__(self, obj, value):
        state = obj.__values__[self]
        if isinstance(state, EXCEPTION):
            if isinstance(state, LOCKED):
                raise ValueError("%r is locked"%(self.name,))
        if value is ABSENT:
            obj.__values__[self] = ABSENT_INSTANCE
        else:
            error, value = self.__validate__(value)
            if error:
                raise ValueError("%r : %r"%(self.name, error))
            obj.__values__[self] = value
        if self.callback:
            self.callback(self, obj, value)

    #def lock__(self):
    #    self.__setattr__ = None

    def set_name__(self, name):
        self.name = name
        return self
    
    def status(self, obj):
        state = obj.__values__[self]
        return state

    def serialize(self, obj):
        value = self.__get__(obj)
        return '%s=%r'%(self.__myname__,value)

class parameter(parameter_base__):
    def __validate__(self, value):
        return None, value

class parameter_numeric__(parameter_base__):
    pass

class parameter_int(parameter_numeric__):
    pass

class parameter_float(parameter_numeric__):
    pass

class parameter_physical(parameter_float):
    pass

class parameter_choice(parameter_base__):
    pass

class parameter_yesno(parameter_base__):
    pass

class parameter_string(parameter_base__):
    pass

class parameter_string(parameter_base__):
    pass
    
class parameter_nested(parameter_base__):
    pass

#############################################################################################################

class ordered_dict_set_once(collections.OrderedDict):
    def __init__(self, bases):
        for b in bases:
            if isinstance(b, meta_parameter_container):
                for p in b.enumerate_parameters():
                    self[p.name] = p

    def __setitem__(self, key, value):
        if key in self:
            raise AttributeError("parameter '%s' defined twice"%key)
        else:
            super(ordered_dict_set_once, self).__setitem__(key, value)

class meta_parameter_container(type):
    @classmethod
    def __prepare__(cls, name, bases):
        return ordered_dict_set_once(bases)
        #return collections.OrderedDict()

    def __new__(cls, name, bases, namespace):
        params = [ o.set_name__(name) for (name,o) in namespace.items() if isinstance(o, parameter_base__)]
        namespace['__parameters__'] = params    
        ##############################################################
        # __slots__ are problematic with multiple inheritance.
        # This little trickery allows multiple inheritance of parameter_container
        # Not sure I'm keeping this.
        if True:
            O = ordered_dict_set_once(bases[1:])
            if O:
                type = meta_parameter_container.__new__(cls, "side_classes", (bases[0],), O)
                bases = (type,)
        ##############################################################
        namespace['__slots__'] = ['__values__']
        return super(meta_parameter_container, cls).__new__(cls, name, bases, namespace)

class parameter_container(metaclass=meta_parameter_container):
    def __init__(self, *args, **kwargs):
        self.__values__ = {}
        for i, p in enumerate(self.enumerate_parameters()):
            if i < len(args):
                v = args[i]
                if p.name in kwargs:                    
                    raise TypeError("__init__() got multiple values for argument '%s'"%p.name)
            else:
                v = kwargs.pop(p.name, ABSENT)
            if v is ABSENT:
                v = p.default
                if v == MUST_INIT:
                    raise TypeError("parameter '%s' must be initialized in the constructor"%p.name)
            self.__values__[p] = ABSENT_INSTANCE
            setattr(self, p.name, v)
                
    @classmethod
    def enumerate_parameters(cls, recursive=True):
        L = []
        if recursive:
            for b in cls.__bases__:
                if isinstance(b, meta_parameter_container):
                    L += b.enumerate_parameters(True)                
        return cls.__parameters__


__all__ = ["parameter_container", "parameter", "parameter_int", "parameter_float"]
