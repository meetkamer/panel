import unittest

from panels.parameter import *

class test_parameter(unittest.TestCase):

    def test1(self):        
    
        with self.assertRaisesRegex(NameError, "is not defined"):
            a = parameter_base__()
            
        with self.assertRaisesRegex(AttributeError, "parameter 'xxx' defined twice"):
            class P(parameter_container):
                xxx = parameter()
                xxx = parameter(label='xxx')

        class P(parameter_container):
            x1 = parameter()
            x2 = parameter(label='x2')
            x3 = parameter(label='x3', default=3)
            x4 = parameter(default=3)

        class Q1(parameter_container):
            x2 = parameter_float()

        class Q2(Q1):
            pass

        class Q3(Q2):
            pass

        with self.assertRaisesRegex(AttributeError, "parameter 'x1' defined twice"):
            class P2(P):
                x1 = parameter_int()

        with self.assertRaisesRegex(AttributeError, "parameter 'x2' defined twice"):
            class P2(P, Q3):
                pass
                
    def test2(self):        
        class P(parameter_container):
            x1 = parameter(default=parameter.ABSENT)
        p = P()
        p.x1 = 'foo'
        with self.assertRaisesRegex(AttributeError, "'P' object has no attribute 'a'"):
            p.a = 1
        with self.assertRaisesRegex(AttributeError, "__delete__"):
            del p.x1

    def test3(self):        
        class P(parameter_container):
            x1 = parameter(default=parameter.ABSENT)
            x2 = parameter(default=3)
            x3 = parameter(default=parameter.ABSENT)
            
        p = P()
        self.assertEqual(p.x2, 3)
        with self.assertRaisesRegex(parameter.ValueError, "'x1' has not been set"):
            p.x1
        with self.assertRaisesRegex(ValueError, "'x1' has not been set"):
            p.x1
        p.x1 = 'foo'
        self.assertEqual(p.x1, 'foo')
        with self.assertRaisesRegex(parameter.ValueError, "'x3' has not been set"):
            p.x3

    def test4(self):        
        '''
        test if parameters come out in the right order in case of multiple inheritance
        '''

        class P1(parameter_container):
            a0 = parameter(default=0)
            a1 = parameter()
            a2 = parameter(default=2)
            a3 = parameter()
            a4 = parameter(default=4)
            
        class P2(P1):
            a5 = parameter()
            a6 = parameter(default=6)
            a7 = parameter()
            a8 = parameter(default=8)

        class P3(parameter_container):
            a9 = parameter()
            a10 = parameter(default=10)
            a11 = parameter()
            a12 = parameter(default=12)

        class P4(P3):
            a13 = parameter()
            a14 = parameter(default=14)
            a15 = parameter()
            a16 = parameter(default=16)

        class P(P2, P4):
            a17 = parameter()
            a18 = parameter(default=18)

        for i, param in enumerate(P.enumerate_parameters()):
            self.assertEqual(i, int(param.name[1:]))
        self.assertEqual(i, 18)

    def test5(self):
        with self.assertRaisesRegex(RuntimeError, r"Unknown argument\(s\): foo"):
            class P(parameter_container):
                x1 = parameter()
                x2 = parameter(default=3)
                x3 = parameter(foo=True)

    def test6(self):
        class P(parameter_container):
            x1 = parameter(default=parameter.ABSENT, doc="first parameter")
            x2 = parameter(default=-3, callback=lambda p, o, x: x**2)
            x3 = parameter(default=parameter.ABSENT)
        
        for i in range(3):
            if i > 1:      
                class Q(P):
                    pass
                P = Q
            p1 = P()
            with self.assertRaisesRegex(parameter.ValueError, "'x1' has not been set"):        
                p1.x1
            self.assertEqual(p1.x2, -3)
            with self.assertRaisesRegex(parameter.ValueError, "'x3' has not been set"):        
                p1.x3

            p1 = P(x1="aa")
            self.assertEqual(p1.x1, "aa")
            self.assertEqual(p1.x2, -3)
            with self.assertRaisesRegex(parameter.ValueError, "'x3' has not been set"):        
                p1.x3        

            p1 = P(x1="aa", x2 = 11)
            self.assertEqual(p1.x1, "aa")
            self.assertEqual(p1.x2, 11)
            with self.assertRaisesRegex(parameter.ValueError, "'x3' has not been set"):        
                p1.x3      

            p1 = P(x3=(), x1=None, x2=1)
            self.assertEqual(p1.x1, None)
            self.assertEqual(p1.x2, 1)
            self.assertEqual(p1.x3, ())

    def test7(self):
        class P1(parameter_container):
            a0 = parameter(default=0)
            a1 = parameter(default=11)
            a2 = parameter(default=2)
            a3 = parameter()
            a4 = parameter("A4", 4, "The A4 parameter")
            def do_it(self):
                self.a3 = self.a4
                self.a4 += 1
                return self.a4

        with self.assertRaisesRegex(TypeError, r"parameter 'a3' must be initialized in the constructor"): 
            p1 = P1()
        
        with self.assertRaisesRegex(TypeError, r"__init__\(\) got multiple values for argument 'a0'"): 
            p1 = P1(2,a0=2)

        with self.assertRaisesRegex(TypeError, r"__init__\(\) got multiple values for argument 'a2'"): 
            p1 = P1(1,2,3,a2=2)
    
        p1 = P1(a1=2, a3=11)
        self.assertEqual(p1.a4, 4)
        p1.do_it()
        self.assertEqual(p1.a3, 4)
        self.assertEqual(p1.a4, 5)

        p2 = P1(6,7,8,9,10)
        self.assertEqual(p2.a0, 6)
        self.assertEqual(p2.a1, 7)
        self.assertEqual(p2.a2, 8)
        self.assertEqual(p2.a3, 9)
        self.assertEqual(p2.a4, 10)

        p2 = P1(6,7,8,a4=10,a3=9)
        self.assertEqual(p2.a0, 6)
        self.assertEqual(p2.a1, 7)
        self.assertEqual(p2.a2, 8)
        self.assertEqual(p2.a3, 9)
        self.assertEqual(p2.a4, 10)

        class P2(P1):
            a5 = parameter(default=25)
            a6 = parameter(default=parameter.ABSENT)
        p2 = P2(6,7,8,9,10,11,12)
        self.assertEqual(p2.a0, 6)
        self.assertEqual(p2.a1, 7)
        self.assertEqual(p2.a2, 8)
        self.assertEqual(p2.a3, 9)
        self.assertEqual(p2.a4, 10)
        self.assertEqual(p2.a5, 11)
        self.assertEqual(p2.a6, 12)

        with self.assertRaisesRegex(TypeError, r"__init__\(\) got multiple values for argument 'label'"):        
            class P2(parameter_container):
                a0 = parameter("A0", label="A0")
        
        with self.assertRaisesRegex(TypeError, r"__init__\(\) got multiple values for argument 'x'"):                
            class X:
                def __init__(self, x):
                    pass
            x = X(1, x=2)

if __name__ == '__main__':
    unittest.main()


